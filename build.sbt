name := "Seabird Medici Extractor"

version := "1.0"

scalaVersion := "2.10.1"

mainClass in (Compile, run) := Some("edu.illinois.ncsa.mmdb.extractor.Main")

resolvers += "Local Maven Repository" at "file:///"+Path.userHome.absolutePath+"/.m2/repository"

libraryDependencies ++= Seq(
    "com.rabbitmq" % "amqp-client" % "3.0.0",
    "medici2" % "receiver" % "0.0.1-SNAPSHOT",
    "org.clapper" % "grizzled-slf4j_2.10" % "1.0.1",
    "org.json4s" %% "json4s-native" % "3.2.4",
    "net.databinder.dispatch" %% "dispatch-core" % "0.10.1"
)

