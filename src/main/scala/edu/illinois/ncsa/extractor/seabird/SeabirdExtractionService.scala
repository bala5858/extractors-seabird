package edu.illinois.ncsa.extractor.seabird

import java.io.File
import java.util.Date
import java.util.ArrayList

import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods.parse
import org.json4s.string2JsonInput

import dispatch.Defaults.executor
import dispatch.Http
import dispatch.StatusCode
import dispatch.as
import dispatch.enrichFuture
import dispatch.host
import dispatch.implyRequestHandlerTuple
import dispatch.implyRequestVerbs
import edu.illinois.ncsa.extractor.seabird.parser.SeabirdParser
import edu.illinois.ncsa.mmdb.extractor.service.ExtractionService
import edu.illinois.ncsa.mmdb.extractor.service.GeostreamExtractionService
import edu.illinois.ncsa.mmdb.extractor.service.GeostreamService
import edu.illinois.ncsa.mmdb.geostream.Geometry
import edu.illinois.ncsa.mmdb.geostream.GeostreamSensor
import edu.illinois.ncsa.mmdb.geostream.GeostreamDataPoint
import edu.illinois.ncsa.mmdb.geostream.GeostreamStream
import grizzled.slf4j.Logging

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

/**
 * Seabird extraction service.
 *
 * @author Luigi Marini
 *
 */
class SeabirdExtractionService extends ExtractionService with Logging with GeostreamExtractionService {

  implicit val formats = DefaultFormats
  
  val HOST = "localhost";
  val PORT = 9001;
  val API_PATH = "api";
  val GEOSTREAMS_PATH = "geostreams";
  val SENSORS_PATH = "sensors";
  
  val ErieFileName = """(er|ER)(\d\d\w?)(SP|SU|sp|su)(\d\d)\.(?:CNV|cnv)""".r
  val HuronFileName = """(hu|HU)(\d\d\w?)(SP|SU|sp|su)(\d\d)\.(?:CNV|cnv)""".r
  val MichiganFileName = """(lm|LM|mi|MI)(\d\d\w?)(SP|SU|sp|su)(\d\d)\.(?:CNV|cnv)""".r
  val OntarioFileName = """(on|ON)(\d\d\w?)(SP|SU|sp|su)(\d\d)\.(?:CNV|cnv)""".r
  val SuperiorFileName = """(su|SU|ls|LS)(\d\d\w?)(SP|SU|sp|su)(\d\d)\.(?:CNV|cnv)""".r
  
  var geostreamService: GeostreamService = _;

  def runExtraction(file: File): Object = {
    info("Parsing seabird file " + file.getAbsolutePath())
    SeabirdParser.readFile(file) match {
      case (metadata, entries) => fn(metadata, entries, file.getName())
    }
    List.empty
  }
  
  def fn(metadata: Map[Symbol, Any], entries: List[Map[String, String]], filename: String) = {
    val sensor = findSensor(metadata: Map[Symbol, Any], filename)
    
    val stream = createStream(sensor, filename);

    sendResponse(sensor.getGeometry, metadata, entries, stream)
    
    /*metadata.get('lat) match {
      case Some(lat) => metadata.get('lng) match {
        case Some(lng) => findNearestSensors(lat.asInstanceOf[Double], lng.asInstanceOf[Double], metadata, filename)
        case None => findAllSensors(metadata, entries, filename)
      }
      case None => findAllSensors(metadata, entries, filename)
    }*/
  }
  
  def findNearestSensors(lat: Double, lng: Double, metadata: Map[Symbol, Any], filename: String) = {
    //TODO: Implement Me.
  }
  
  def findSensor(metadata: Map[Symbol, Any], filename: String): GeostreamSensor = {
    val sensors = this.geostreamService.getSensors();
    
    val stationName = filename match {
      case ErieFileName(lake, station, season, year) => "ER" + station
      case HuronFileName(lake, station, season, year) => "HU" + station
      case MichiganFileName(lake, station, season, year) => "MI" + station
      case OntarioFileName(lake, station, season, year) => "ON" + station
      case SuperiorFileName(lake, station, season, year) => "SU" + station
      case _ => throw new Exception("Unrecognized file name format.")
    }

    val recognizedSensor = sensors.toList find (sensor => sensor.getName().equalsIgnoreCase(stationName))
    
    recognizedSensor match {
      case Some(sensor) => sensor
      case None => throw new Exception("Sensor not found.")
    }
  }
  
  def sendResponse(geometry: Geometry, metadata: Map[Symbol, Any], entries: List[Map[String, Object]], stream: GeostreamStream) = {
    for(entry <- entries) yield {
      val start_date = metadata('start_time).asInstanceOf[Date]
      
      val DepthKey = """.*(d|D)epth.*""".r
      entry collectFirst {case (DepthKey(d), v) => v} match {
       	case Some(value) => val coor = List[java.lang.Double](geometry.getCoordinates().get(0), geometry.getCoordinates().get(1), value.asInstanceOf[String].toDouble).asJava; geometry.setCoordinates(coor)
       	case None => println("Couldn't find a depth value.")
      }
      this.geostreamService.createDataPoint(start_date, start_date, geometry, entry.asJava, "Point", stream.getId().toString)
    }
  }
  
  def createStream(sensor: GeostreamSensor, filename: String) = {
    val geometry = new Geometry();
    val coordinates = new ArrayList[java.lang.Double](3);
    coordinates.add(new java.lang.Double(0));
    coordinates.add(new java.lang.Double(0));
    coordinates.add(new java.lang.Double(0));
    
    geometry.setCoordinates(coordinates)
    geometry.setType("Point");
    
    this.geostreamService.createStream(new Date(), geometry, sensor.getName(), Map("file" -> filename), sensor.getId().toString, "Feature")
  }
  
  def setGeostreamService(geostreamService: GeostreamService):Unit = {
    this.geostreamService = geostreamService;
  }
}