/**
 *
 */
package edu.illinois.ncsa.extractor.seabird

import org.springframework.util.ErrorHandler
import grizzled.slf4j.Logging

/**
 * Used to log Spring container errors.
 * 
 * @author Luigi Marini
 *
 */
class ContainerErrorHandler extends ErrorHandler with Logging {

  def handleError(t:Throwable) {
    error("Error receiving message: ", t)
  }
}