/**
 *
 */
import dispatch._, Defaults._
import grizzled.slf4j.Logging

/**
 * Testing API.
 * 
 * @author Luigi Marini
 *
 */
object GeostreamingAPI extends Logging {

  def main(args: Array[String]): Unit = {
    info("Testing geostreaming api")
    val geostream = host("localhost", 9000) / "api" / "geostreams" <<? Map("key" -> "letmein") <:< Map("Content-type" -> "application/json")
    val addDatapoint = geostream / "1" << """{"start_time":"2013-03-25 00:00:00","end_time":"2013-03-25 00:00:00","data":{"value":"test"},"geog":{"type":"Point","coordinates":[-88.20727,40.110588,-5]}}""" POST
    val addedDatapoint = Http(addDatapoint OK as.String)

    addedDatapoint.either() match {
      case Right(content) => info("addedDatapoint Content: " + content)
      case Left(StatusCode(404)) => info("addedDatapoint Not found")
      case Left(StatusCode(code)) => info("addedDatapoint Some other code: " + code.toString)
    }
  }

}