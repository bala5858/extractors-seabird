/*******************************************************************************
 * University of Illinois/NCSA
 * Open Source License
 *
 * Copyright (c) 2013, NCSA.  All rights reserved.
 *
 * Developed by:
 * Cyberenvironments and Technologies (CET)
 * http://cet.ncsa.illinois.edu/
 *
 * National Center for Supercomputing Applications (NCSA)
 * http://www.ncsa.illinois.edu/
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal with the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimers.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimers in the
 *   documentation and/or other materials provided with the distribution.
 * - Neither the names of CET, University of Illinois/NCSA, nor the names
 *   of its contributors may be used to endorse or promote products
 *   derived from this Software without specific prior written permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *******************************************************************************/
package edu.illinois.ncsa.extractor.seabird.parser

import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
//import java.util.HashMap

import scala.collection.breakOut
import scala.collection.immutable.HashMap
import scala.io.BufferedSource
import scala.io.Source

import edu.illinois.ncsa.extractor.seabird.model.Feature
import edu.illinois.ncsa.extractor.seabird.model.GeoJSON
import edu.illinois.ncsa.extractor.seabird.model.GeoJSON
import grizzled.slf4j.Logging

/**
 * Parser for seabird file format .cnv
 * 
 * @author Nicholas Tenczar <tenczar2@illinois.edu>
 *
 */
object SeabirdParser extends Logging {
  
  val Star = """\*+ (.+)""".r
  val Hash = """# (.+)""".r
  val Table ="""\s+(.*)""".r
  
//  val number = """[\+-]?(?:0|[1-9]\d*)(?:\.\d*)(?:[eE][\+-]?\d+)?"""
  
  val Span = """.*span (\d+) =\s+(.*),\s+(.*)""".r
  val Name = """.*name (\d+) =\s+(.*)\: (.*)(\[.*\])?""".r
  
  val StartTime = """.*start_time = (.*)(?:\s\[NMEA time\, header\])?""".r
  val Station = """.*Station:\s(.*)""".r
  val NQuan = """.*nquan =\s(.*)""".r
  
  val LatLng = """.*(:?NMEA\s)?(Latitude|Longitude) = (.*)""".r
  val DMS = """([-|+|N|E|S|W]?)(\d+)[ +|\u00B0](\d+)[ |'](\d+)"?""".r
  val MinDec = """(\d+) (\d+\.\d+) (N|E|S|W)""".r
  val Decimal = """([-|+])?(\d+\.\d+)""".r
  
  val dateParser = new SimpleDateFormat("MMM d yyyy HH:mm:ss")
  
  def main(args: Array[String]): Unit = {
    readFile(new File(args(0)))
  }
  
  def readFile(file: File): (Map[Symbol, Any], List[Map[String, String]]) = {
    val content = Source.fromFile(file.getAbsolutePath())
    parse(content.getLines(), HashMap.empty[Symbol, Any], Nil)
  }
  
  def parse(iter:Iterator[String], metadata: Map[Symbol, Any], params: List[String]): (Map[Symbol, Any], List[Map[String, String]]) = {
    if(!iter.hasNext)
      throw new Exception() // TODO: FIX ME
      
    val value = iter.next
    if(value == "*END*") {
      val reorderParams = params.reverseMap(param => param.replace("null", ""))
      
      val valsParsed = (for(values <- iter) yield {
        (reorderParams zip (values.trim split "\\s+")).toMap
      }).toList
        
      return (metadata, valsParsed);
    }
    
    value match {
      case StartTime(time) => parse(iter, metadata + ('start_time -> dateParser.parse(time)), params)
      case LatLng(coord, value) => val parsedValue = parseLatLng(value); if(coord contains "Latitude") parse(iter, metadata + ('lat -> parsedValue), params) else parse(iter, metadata + ('lng -> parsedValue), params)
      case Name(num, abbr, descrip, units) => parse(iter, metadata, (descrip + units) :: params)
      case NQuan(quantity) => parse(iter, metadata + ('nquan -> quantity), params)
      case _ => parse(iter, metadata, params)
    }
  }
  
  def parseLatLng(value: String): Option[Double] = {
    value match {
      case DMS(degrees, minutes, seconds) => Some(dmsToDecimal(degrees.toDouble, minutes.toDouble, seconds.toDouble))
      case MinDec(degrees, minDec, dir) => Some(minDecToDecimal(degrees.toDouble, minDec.toDouble, dir))
      case Decimal(degrees) => Some(degrees.toDouble)
      case _ => None
    }
  }
    
  def dmsToDecimal(degrees: Double, minutes: Double, seconds: Double): Double = {
    degrees + (minutes / 60) + (seconds / 3600)
  }
  
  def minDecToDecimal(degrees: Double, minDec: Double, dir: String): Double = {
    if (dir == "W" || dir == "S") {
      -1 * degrees + (minDec / 60)
    } else {
      degrees + (minDec / 60)
    }
  }
}